/**********************************************
* BefExtract.groovy for setting bind variables
***********************************************/

//Maps used to convert MMM value to integer to use in SQL query
Map ConvertStartWeeks = ["Aug":"1","Sep":"5","Oct":"9","Nov":"14","Dec":"18","Jan":"22","Feb":"27","Mar":"31","Apr":"35","May":"40","Jun":"44","Jul":"48"]
Map ConvertEndWeeks = ["Aug":"4","Sep":"8","Oct":"13","Nov":"17","Dec":"21","Jan":"26","Feb":"30","Mar":"34","Apr":"39","May":"43","Jun":"47","Jul":"53"]
Map ConvertMonths = ["Aug":"01","Sep":"02","Oct":"03","Nov":"04","Dec":"05","Jan":"06","Feb":"07","Mar":"08","Apr":"09","May":"10","Jun":"11","Jul":"12"]

//Method for converting and setting bind variables for data loads to BHPlan
def BHPlan(ConvertMonths) {
	Map updatedBindVariables = [:]
	bindVariables = agentAPI.getBindVariables()
	bindVariables.each { entry ->
		if ((entry.key).toString().contains("MONTH")) {
			updatedBindVariables.put(entry.key, ConvertMonths.get((entry.value).toString()))
			agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${ConvertMonths.get((entry.value).toString())}")
		} else if ((entry.key).toString().contains("FISCALYEAR")) {
			//adding 1 to the year to get to fiscal year for Aug-Dec
			switch (bindVariables.get("STARTMONTH").toString()) {
				case ("Aug"):
					updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}");
					break;
				case ("Sep"):
					updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}");
					break;
				case ("Oct"):
					updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}");
					break;
				case ("Nov"):
					updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}");
					break;
				case ("Dec"):
					updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}");
					break;
				default :
					updatedBindVariables.put(entry.key, entry.value);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: $entry.value");
					break;
			}
		} else {
			updatedBindVariables.put(entry.key, entry.value);
			agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: $entry.value");
			}
	}
	agentAPI.setBindVariables(updatedBindVariables)
}

//Method for converting and setting bind variables for data loads to BHPlan2
def BHPlan2(ConvertStartWeeks, ConvertEndWeeks) {
	Map updatedBindVariables = [:]
	bindVariables = agentAPI.getBindVariables()
	bindVariables.each { entry ->
		if ((entry.key).toString().contains("STARTWEEK")) {
			updatedBindVariables.put(entry.key, ConvertStartWeeks.get((entry.value).toString()))
			agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${ConvertStartWeeks.get((entry.value).toString())}")
		} else if ((entry.key).toString().contains("ENDWEEK")) {
			updatedBindVariables.put(entry.key, ConvertEndWeeks.get((entry.value).toString()))
			agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${ConvertEndWeeks.get((entry.value).toString())}")
		} else if ((entry.key).toString().contains("FISCALYEAR")) {
			//adding 1 to the year to get to fiscal year for Aug-Dec
			switch (bindVariables.get("STARTWEEK").toString()) {
				case ("Aug"):
					updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}");
					break;
				case ("Sep"):
					updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}");
					break;
				case ("Oct"):
					updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}");
					break;
				case ("Nov"):
					updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}");
					break;
				case ("Dec"):
					updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}");
					break;
				default :
					updatedBindVariables.put(entry.key, entry.value);
					agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: $entry.value");
					break;
			}
		}			
	}
	agentAPI.setBindVariables(updatedBindVariables)
}

//Logging bind variables and values as set in Data management
println ("--------------------------")
agentAPI.logInfo("--------------------------")
println "Starting bind variables"
agentAPI.logInfo("Starting bind variables")
agentAPI.getBindVariables().each { entry ->
	println "Bind variable name: $entry.key Value: $entry.value"
	agentAPI.logInfo("Bind variable name: $entry.key Value: $entry.value")
}
println ("--------------------------")
agentAPI.logInfo("--------------------------")


//Checking if there are more than 1 bind variables
if (agentAPI.getBindVariables().size() > 1) {
	//Checking target application since BHPlan is monthly and BHPlan2 is weekly
	switch(agentContext.get("TARGET_APPLICATION").toString()) {
		case ("BHPlan"):
			BHPlan(ConvertMonths);
			break;
		case ("BHPlan2"):
			BHPlan2(ConvertStartWeeks, ConvertEndWeeks);
			break;
		default :
			break;
	}
} else {
	Map updatedBindVariables = [:]
	agentAPI.getBindVariables().each { entry ->
	updatedBindVariables.put(entry.key, ((entry.value).toInteger()) + 1)
	agentAPI.logInfo("Name: $entry.key Old Value: $entry.value New Value: ${((entry.value).toInteger()) + 1}")
	}
	agentAPI.setBindVariables(updatedBindVariables)
}

//Logging bind variables and values after conversion process
println ("--------------------------")
agentAPI.logInfo("--------------------------")
println "Updated bind variables"
agentAPI.logInfo("Updated bind variables")
agentAPI.getBindVariables().each{ entry ->
	println "Bind variable name: $entry.key Value: $entry.value"
	agentAPI.logInfo("Bind variable name: $entry.key Value: $entry.value")
}
println ("--------------------------")
agentAPI.logInfo("--------------------------")



				