/**************************
**function to alter format and move file
***************************/
def alterPlanningFormat (srcFile, destFile) {
	File file = new File(srcFile)
	println "Orig File: $file"
	File newFile = new File(destFile)
	if (newFile.exists()) {newFile.delete()}
		file.eachLine { line ->
			 newFile.append("${line.replace('"','').replace('#missing','')}\n")
			}
	println "New File: $newFile"
	file.delete()
}
/*********************
**begin script
**********************/
//set needed variables//
	Date date = new Date()
	String archiveDate = date.format("ddMMyyyyHHmm")
	String srcFile = args[0]
	String destFile = args[1]
	destFile = destFile.minus(destFile.substring(destFile.lastIndexOf(".")))+archiveDate+destFile.substring(destFile.lastIndexOf("."))
	String Trigger = new File(destFile).getParent() + /\TRIGGERFILE.txt/
//Alter format and move file//
boolean alteredFormat = alterPlanningFormat(srcFile, destFile)
//create Trigger file//
if (alteredFormat) {
	new File(Trigger).text = "Trigger"
	}


