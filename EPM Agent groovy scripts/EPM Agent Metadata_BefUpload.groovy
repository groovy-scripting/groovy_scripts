/*****************************************
* BefUpload.groovy for Metadata Process
******************************************/
//import java.text.simpleDateFormat

Date date = new Date()
String archiveDate = date.format("MM.dd.yyyy_HH.mm.ss")
if (agentContext.get("INTEGRATION").toString() != "Rate_Code_IntAg_rul") { 
	/**
	* Skip all remaining Data Managemnt actions after the metadata file has been extracted from the ADW.
	**/
	agentAPI.skipAction('true')

	/**
	* Set up variables for to rename file and pass paramaters to the EPM Automate script
	**/
	String Dimension = agentContext.get("INTEGRATION").replace("BHPlan2",'').trim()
	String importJob = "import"+Dimension
	File dataFileName = new File(agentContext.get("DATAFILENAME"))
	File dimensionFileName = new File(/..\..\..\EPM_Agent\Metadata_EPMAgentData\data\${Dimension}.txt/)
	File archiveFileName = new File(/..\..\Metadata_EPMAgentData\data\${Dimension}_${archiveDate}.txt/)

	/**
	* Rename data file from [jobId].dat to [dimension].txt.
	**/
	if (dimensionFileName.exists()) { dimensionFileName.delete() }
	boolean successfulRename = dataFileName.renameTo(dimensionFileName)
	println "metadata file renamed: $successfulRename"
	agentAPI.logInfo("metadata file renamed: $successfulRename")
	agentAPI.logInfo("${dimensionFileName.getParent()}")
	println "${dimensionFileName.getParent()}"
	/**
	* Execute EPM Automate script to upload and import the dimension metadata file
	**/
	String cmd
	if (successfulRename) {
		if (agentContext.get("INTEGRATION").contains("BHPlan2")) {
			cmd = """cmd.exe /c "..\\..\\Metadata_EPMAgentData\\EPM_Scripts\\BHPlan2_Upload_ImportMetadata.cmd" ${dimensionFileName.getParentFile()} $importJob $Dimension"""
		} else {
			cmd = """cmd.exe /c "..\\..\\Metadata_EPMAgentData\\EPM_Scripts\\Upload_ImportMetadata.cmd" ${dimensionFileName.getParentFile()} $importJob $Dimension"""
		}
		println("Executing "+cmd)
		agentAPI.logInfo("Executing "+cmd)
		Runtime runtime=Runtime.getRuntime();
		Process proc=runtime.exec(cmd);
		BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		String line;
		while((line = reader.readLine()) != null){
			System.out.println(line);
			agentAPI.logInfo(line);
			}
		proc.waitFor();

		//dimensionFileName.renameTo(archiveFileName)
	} else {
		agentAPI.logError("Dimension file not renamed for upload and import... exiting process")
		return
		}
} else {
	return
	}
	