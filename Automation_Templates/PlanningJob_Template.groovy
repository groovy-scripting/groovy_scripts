def executePlanningJob(String connection, String appName, String vJobType, String vJobName, Map<String, String> Parameters) {
//create JSON body for Planning job
	def body = new JSONObject()
	.put("jobType",vJobType)
	.put("jobName",vJobName)
	.put("parameters",Parameters)
	.toString()

    println "JSON Body:$body"
    println "---"

	//call REST API to run Planning job
	HttpResponse<String> calcResponse = operation.application
	.getConnection(connection)
	.post("/rest/v3/applications/$appName/jobs")
	.body(body)
	.asString()

	if ((200..299).contains(calcResponse.status as int)) {
		throwVetoException("Error occurred running DLR status code=$calcResponse.status status text=$calcResponse.statusText")
	} else {
        //extract values from JSON response
        def json = new JSONObject(calcResponse.body)
        def status = json.getInt("status")
        def ruleStatus = json.getString("descriptiveStatus")
        def processID = json.getInt("jobId")
        def jobName = json.getString("jobName")

        //check status of job for completion
        while(status < 0){
            calcResponse = operation.application
            .getConnection(connection)
            .get("/rest/v3/applications/$appName/jobs/$processID")
            .asString()
    
            json = new JSONObject(calcResponse.body)
            status = json.getInt("status")
            ruleStatus = json.getString("descriptiveStatus")
            processID = json.getInt("jobId")
            jobName = json.getString("jobName")
        }

        //check status and send email
        if(status!=0){
            String subject = "$jobName: $ruleStatus"
            String emailBody = "problem running $jobName with process id $processID, status returned:$ruleStatus"
            sendMail(subject, emailBody)
            throwVetoException("problem running $jobName with process id $processID, status returned:$ruleStatus")
        } else {
            /*String subject = "$jobName: $ruleStatus"
            String emailBody = "$jobName with process id $processID, status returned:$ruleStatus"
            sendMail(subject, emailBody)*/
            println "Job Status: $ruleStatus"
        }
    }
}