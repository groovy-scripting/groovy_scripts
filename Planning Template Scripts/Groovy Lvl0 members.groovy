Set<String> Accounts = []
Cube cube = operation.application.getCube("")
	Dimension AccountDim = operation.application.getDimension("WorkProgram", cube)
	// loop through all base level accounts to create a list of Lvl0 accounts
		def Lvl0Acct = AccountDim.getEvaluatedMembers("ILvl0Descendants(Account)", cube) as String[]
		for (i=0; i<Lvl0Acct.size(); i++) {
			Accounts << Lvl0Acct[i] }
		