
def FlagGrid
def Flag 
Cube cube = operation.application.getCube("Main")
DataGridDefinitionBuilder builder = cube.dataGridDefinitionBuilder()
// Build grid to check EACProposal
builder.addPov(['Scenario','Version','Year','Measure','Entity'],[['Actual'],['Working'],['FY20'],['NoMeasure'],['NoEntity']])
builder.addColumn(['Period'], [ ['Jan'] ])
builder.addRow(['Account'], [ ['Sales'] ])
DataGridDefinition gridDefinition = builder.build()
// Load a data grid from the specified cube with the specified grid POV, boolean enforce user security. 
FlagGrid = cube.loadGrid(gridDefinition, false)
// Check the flag value
FlagGrid.dataCellIterator().each {
	Flag = it.getData()
 }

if(Flag == 1) {}