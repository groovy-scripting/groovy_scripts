Cube cube = operation.application.getCube("")
Dimension YearDim = operation.application.getDimension("Year", cube)
def RSiblingsCurrentYear = YearDim.getEvaluatedMembers("IRSiblings($SubStVar_CurrentYear)", cube) as Set
def YearRange1 = [] as Set
RSiblingsCurrentYear.each {
YearRange1 << it.toString()
}
def RSiblingsEndYear = YearDim.getEvaluatedMembers("RSiblings($SubStVar_EndYear)", cube) as Set
def YearRange2 = [] as Set
RSiblingsEndYear.each {
YearRange2 << it.toString()
}
YearRange2.each {
YearRange1.remove(it)
}
//YearRange1 = YearRange1 - YearRange2
Years = YearRange1

