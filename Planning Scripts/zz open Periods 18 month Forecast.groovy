/*RTPS: {RTP_Scenario}*/

// Dynamically get the list of weeks based on the application's fiscal year 
Cube cube = operation.application.getCube("Demand") 
Dimension periodDim = operation.application.getDimension("Period", cube) 
def weeks = periodDim.getEvaluatedMembers("ILvl0Descendants(YearTotal)", cube)

// Create delimited list for methods that don't require a List object
def useWeeksString = """\"${weeks.join('", "')}\""""

// Construct a string builder - Holds the calculation script sent to Essbase
StringBuilder essCalc = StringBuilder.newInstance()
StringBuilder essCalcDetail = StringBuilder.newInstance()

// Get the Scenario from RTP_Scenario
String sScenario = rtps.RTP_Scenario.getMember()

// Get the week from the sub var
def actWeek = operation.application.getSubstitutionVariableValue("CurrWeek").toString()
def fcstStartWeek = operation.application.getSubstitutionVariableValue("FcstStartWk").toString()
def fcstEndWeek = operation.application.getSubstitutionVariableValue("FcstEndWk").toString()
def fcstYr2 = operation.application.getSubstitutionVariableValue("FcstYr2").toString()
def fcstYr3 = operation.application.getSubstitutionVariableValue("FcstYr3").toString()

// Parse and define as integers for conditionals
int startWkValue = fcstStartWeek.substring(1) as Integer
int endWkValue = fcstEndWeek.substring(1) as Integer
  
// Create dynamic list of weeks based on RTP_Scenario
String useWeeks = ";"
if(sScenario.contains('Plan')){
  // This will create a list of all weeks - assuming the the plan is for all weeks
  useWeeks = useWeeksString 
  println "plan"
  essCalcDetail << """
	FIX($useWeeks, "")
    	%Script(name:="Demand.DistChanges.WeeklySpread.CPG.RF",application:="BHPlan2",plantype:="Demand")
    ENDFIX
  """
  }
else if(sScenario.contains("Forecast")){
  println "forecast"
  if(startWkValue < 27){
  essCalcDetail << """
	FIX(${fcstStartWeek}:W53, &FcstStartYr)
    	%Script(name:="Demand.DistChanges.WeeklySpread.CPG.RF",application:="BHPlan2",plantype:="Demand")
    ENDFIX
    FIX(W1:${fcstEndWeek}, $fcstYr2)
    	%Script(name:="Demand.DistChanges.WeeklySpread.CPG.RF",application:="BHPlan2",plantype:="Demand")
    ENDFIX
  """
   }
  else if (endWkValue == 53){
  essCalcDetail << """
	FIX(${fcstStartWeek}:W53, &FcstStartYr)
    	%Script(name:="Demand.DistChanges.WeeklySpread.CPG.RF",application:="BHPlan2",plantype:="Demand")
    ENDFIX
    FIX($useWeeksString, $fcstYr2)
    	%Script(name:="Demand.DistChanges.WeeklySpread.CPG.RF",application:="BHPlan2",plantype:="Demand")
    ENDFIX
  """
  }
  else {
  essCalcDetail << """
	FIX(${fcstStartWeek}:W53, &FcstStartYr)
    	%Script(name:="Demand.DistChanges.WeeklySpread.CPG.RF",application:="BHPlan2",plantype:="Demand")
    ENDFIX
    FIX($useWeeksString, $fcstYr2)
    	%Script(name:="Demand.DistChanges.WeeklySpread.CPG.RF",application:="BHPlan2",plantype:="Demand")
    ENDFIX
    FIX(W1:${fcstEndWeek}, $fcstYr3)
    	%Script(name:="Demand.DistChanges.WeeklySpread.CPG.RF",application:="BHPlan2",plantype:="Demand")
    ENDFIX
  """
  }
  }
else{
  // This will create a list of all actual weeks
  useWeeks = "W1:${actWeek}"
  println "actual"
  }
  
essCalc << """
${essCalcDetail}
"""
println essCalc
return essCalc