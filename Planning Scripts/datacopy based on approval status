/*RTPS:*/

//Custom function to check valid response from REST call
def checkRequest( HttpResponse<String> response ) {
    if(!(200..299).contains(response.status) || !response.headers?.getFirst('Content-Type').matches("application/json.*")){
        if(!(200..299).contains(response.status))
            throwVetoException("Error occured: " + response.statusText)
        if(!response.headers?.getFirst('Content-Type').matches("application/json.*"))
            throwVetoException("Error occured: Unexpected content type in response: ${response.headers?.getFirst('Content-Type')}")
    }
    else{
        return true
    }
}

//REST API call to PUH 
HttpResponse<String> response = operation.application.getConnection("Approvals").post("/HyperionPlanning/rest/v3/applications/DCAT/planningunits").queryParam("q", '{"scenario":"SC.Plan","version":"VR.Submitted"}').asString()
println ("Response: $response.body")

//Call custom function check
checkRequest(response)

// Gets the response from teh REST request and converts it into a map
Map<String,?> jsonResponse = new JsonSlurper().parseText(response.body) as Map

// Get the list of all items with a status of planning units
List<String> puNameRelative = jsonResponse.items.findAll { it['status'] == 'Approved' }.collect{"@RELATIVE(\"${it['puName']}\",0)".toString()} as List
List<String> puNameDescendants = jsonResponse.items.findAll { it['status'] == 'Approved' }.collect{"@IDESCENDANTS(\"${it['puName']}\")".toString()} as List

//Testing Purposes
//puNameRelative = ["@RELATIVE(\"BLK.50002\", 0)"]
//puNameDescendants = ["@IDESCENDANTS(\"BLK.50002\")"]

// Run if there are any elements to execute
if(puNameRelative.size() >= 1){
    println puNameRelative.join(",")
    println puNameDescendants.join(",")
    def essCalc = new StringBuilder()
    essCalc << 
"""
/***********************************************************************************
Author: Peloton Group
Purpose: Allows CFP&A to move submitted and approved CV data to the approved version
Type: Business Rule

Process:
    1) Clear existing Approved data
    2) Copy Submitted to Approved
    3) Aggregate Block

SubVars:
	1) &PlanYr
	2) &PlanEndYr

RTPs:
	1) Block

Modified Date: Modifications Log:
09/28/2020     	1) Created
11/03/2020		2) Swapped out Source reference
11/18/2020		3) Move Capital reserve from Working version to Approved, skips formal approval process, user needs to select Block None
11/30/2020		4) Added approval summary accounts, changing status to approved
**********************************************************************************/

SET AGGMISSG ON;
SET UPDATECALC OFF;
SET COPYMISSINGBLOCK OFF;
SET EMPTYMEMBERSETS ON;

FIX ("BaseData")
	FIX(@RELATIVE("CO.PRIM_EXT_SG",0), @RELATIVE("Finance_Geo",0), @RELATIVE("Organization",0), @RELATIVE("SR.Tot.Source",0), "SR.No.Source", @RELATIVE("SC.Plan",0), &PlanYr:&PlanEndYr)
        FIX(${puNameRelative.join(",")})

            /* Clear existing Approved data */
            CLEARDATA "VR.Approved";

            /* Copy Submitted to Approved */
            DATACOPY "VR.Submitted" TO "VR.Approved";

            /*Capital Reserve exception*/
            FIX("AC.CapRes")
                DATACOPY "VR.Working" TO "VR.Approved";
            ENDFIX    	
        ENDFIX
	
        /* Aggregate Block */
        FIX ("VR.Approved")
            AGG ("Block");
        ENDFIX
	ENDFIX
    
    /*Assign approver stat accounts*/
    FIX("FGEO.NONE", "CC.None", "SR.No.Source", "CO.NONE", "SC.Case.1", &PlanYr, ${puNameDescendants.join(",")}, "VR.Submitted", "BegBalance")
    	SET CREATENONMISSINGBLK ON;
        	"STC.Submitter"(
                /*Assign to upper level member but do not allow accounts to be aggregated*/
                IF(@ISUDA("Block", "Approval_Pt") AND @ISLEV("Block", 0))
                    "STC.Proj_Status" = [[Status.Approved]];
                ELSEIF(@ISUDA("Block", "Approval_Pt") AND NOT @ISLEV("Block", 0))
                    "STC.Proj_Status"->@MEMBER(@CONCATENATE(@NAME(@CURRMBR("Block")),".PV")) = [[Status.Approved]];
                ENDIF
            ) 
        SET CREATENONMISSINGBLK OFF;
    ENDFIX    
ENDFIX
"""   
println essCalc

//Cube line needed for launching from Calc Mgr; otherwise rule needs to be deployed with rule.cube
Cube cube = operation.application.getCube("DCATPLN")
cube.executeCalcScript(essCalc.toString())

}
else{
    println 'nothing to do'
}